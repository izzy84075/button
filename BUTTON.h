#ifndef __BUTTON_H__
#define __BUTTON_H__

#include <stdint.h>
#include <stdbool.h>

#include "BUTTON_Conf.h"

void BUTTON_Tick1ms(void);

bool BUTTON_CurrentlyTouched(void);
bool BUTTON_Touched(void);
bool BUTTON_Released(void);
bool BUTTON_Held(void);

void BUTTON_AckTouch(void);
void BUTTON_AckRelease(void);
void BUTTON_AckHold(void);
void BUTTON_AckAll(void);

#endif
